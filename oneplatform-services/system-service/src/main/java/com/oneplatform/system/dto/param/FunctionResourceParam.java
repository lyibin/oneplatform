package com.oneplatform.system.dto.param;

import javax.validation.constraints.NotBlank;

public class FunctionResourceParam {

	private Integer id;

	/**
	 * 父ID
	 */
	private Integer parentId;

	/**
	 * 资源名称
	 */
	@NotBlank(message = "功能资源名称不能为空")
	private String name;

	private String code;
	
	private String router;

	private String type;

	private String viewPath;

	private String icon;

	private Integer sort;

	private boolean display = true;
	private Boolean isOpenAccess = Boolean.FALSE;
	
	private Integer bindApiId;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getRouter() {
		return router;
	}
	public void setRouter(String router) {
		this.router = router;
	}
	public String getViewPath() {
		return viewPath;
	}
	public void setViewPath(String viewPath) {
		this.viewPath = viewPath;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public boolean isDisplay() {
		return display;
	}
	public void setDisplay(boolean display) {
		this.display = display;
	}
	
	public Boolean getIsOpenAccess() {
		return isOpenAccess;
	}
	public void setIsOpenAccess(Boolean isOpenAccess) {
		this.isOpenAccess = isOpenAccess;
	}
	public Integer getBindApiId() {
		return bindApiId;
	}
	public void setBindApiId(Integer bindApiId) {
		this.bindApiId = bindApiId;
	}

}