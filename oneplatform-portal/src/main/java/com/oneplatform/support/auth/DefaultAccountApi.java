package com.oneplatform.support.auth;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mendmix.common.MendmixBaseException;
import com.mendmix.common.http.HttpRequestEntity;
import com.mendmix.gateway.api.AccountApi;
import com.mendmix.gateway.model.AccountScope;

@Service
public class DefaultAccountApi implements AccountApi {

	@Value("${user.service.baseUrl:http://oneplatform-user-svc}/validate")
	private String userValidateUrl;
	
	@Value("${user.service.baseUrl:http://oneplatform-user-svc}/scopes")
	private String userScopeUrl;

	@Override
	public LoginUserInfo validateAccount(String accountName, String password) throws MendmixBaseException {
		LoginUserInfo authUser;
		Map<String, String> param = new HashMap<>(2);
		param.put("account", accountName);
		param.put("password", password);
		authUser = HttpRequestEntity.post(userValidateUrl).body(param).backendInternalCall().execute().toObject(LoginUserInfo.class);
		return authUser;
	}

	@Override
	public List<AccountScope> findAccountScopes(String accountId) {
		List<AccountScope> scopes = HttpRequestEntity.get(userScopeUrl).queryParam("userId", accountId).backendInternalCall().execute().toList(AccountScope.class);
		return scopes;
	}

}
