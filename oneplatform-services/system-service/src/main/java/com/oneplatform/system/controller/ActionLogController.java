package com.oneplatform.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mendmix.common.annotation.ApiMetadata;
import com.mendmix.common.model.Page;
import com.mendmix.common.model.PageQueryRequest;
import com.mendmix.common.util.BeanUtils;
import com.mendmix.logging.actionlog.ActionLog;
import com.mendmix.logging.actionlog.ActionLogQueryParam;
import com.mendmix.mybatis.plugin.pagination.PageExecutor;
import com.oneplatform.system.dao.entity.ActionLogEntity;
import com.oneplatform.system.dao.mapper.ActionLogEntityMapper;

@RestController
@RequestMapping("actionlog")
public class ActionLogController {

	@Autowired
	private ActionLogEntityMapper actionLogMapper;
	
	@ApiMetadata(intranetAccess = true,actionLog = false)
	@PostMapping("add")
	public void addActionLog(@RequestBody ActionLogEntity entity) {
		actionLogMapper.insertSelective(entity);
	}
	
	@ApiMetadata(intranetAccess = true,actionLog = false)
	@PostMapping("list")
	public Page<ActionLogEntity> pageQuery(@RequestBody PageQueryRequest<ActionLogQueryParam> param) {
		return PageExecutor.pagination(param.asPageParam(), () -> actionLogMapper.findListByQueryParam(param.getExample()));
	}
	
	@ApiMetadata(intranetAccess = true,actionLog = false)
	@GetMapping("details")
	public ActionLog getDetailsById(@RequestParam("id") String id) {
		ActionLogEntity entity = actionLogMapper.selectByPrimaryKey(id);
		return BeanUtils.copy(entity, ActionLog.class);
	}
}
