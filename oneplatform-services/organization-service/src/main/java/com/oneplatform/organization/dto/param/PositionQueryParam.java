package com.oneplatform.organization.dto.param;

/**
 * 
 * <br>
 * Class Name   : DepartmentQueryParam
 */
public class PositionQueryParam {

    private String name;
    private String code;
    private String type;
	private String departmentId;
    private Boolean enabled;
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	
    
}