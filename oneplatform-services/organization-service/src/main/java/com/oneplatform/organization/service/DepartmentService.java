package com.oneplatform.organization.service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mendmix.common.MendmixBaseException;
import com.mendmix.common.util.BeanUtils;
import com.mendmix.common.util.DigestUtils;
import com.oneplatform.organization.dao.entity.DepartmentEntity;
import com.oneplatform.organization.dao.mapper.DepartmentEntityMapper;
import com.oneplatform.organization.dao.mapper.StaffDepartmentEntityMapper;
import com.oneplatform.organization.dao.mapper.StaffEntityMapper;
import com.oneplatform.organization.dto.Department;
import com.oneplatform.organization.dto.param.DepartmentQueryParam;

/**
 * 
 * <br>
 * Class Name : DepartmentService
 */
@Service
public class DepartmentService {

	@Autowired
	private DepartmentEntityMapper departmentMapper;
	@Autowired
	private StaffEntityMapper staffMapper;
	@Autowired
	private StaffDepartmentEntityMapper staffDepartmentEntityMapper;

	public String addDepartment(DepartmentEntity entity) {

		String code = DigestUtils.md5Short(UUID.randomUUID().toString());
		entity.setCode(code);
		if (StringUtils.isNotBlank(entity.getParentId())) {
			DepartmentEntity parent = departmentMapper.selectByPrimaryKey(entity.getParentId());
			if (parent == null || !parent.getEnabled()) {
				throw new MendmixBaseException("父级部门不存在或已禁用");
			}
			entity.setFullCode(String.format("%s_%s", parent.getCode(), code));
			entity.setFullName(String.format("%s_%s", parent.getName(), entity.getName()));
		} else {
			if(departmentMapper.findTopDepartment() != null) {
				throw new MendmixBaseException("已存在顶级部门");
			}
			entity.setFullCode(code);
			entity.setFullName(entity.getName());
		}

		departmentMapper.insertSelective(entity);
		return entity.getId();
	}

	public void updateDepartment(DepartmentEntity entity) {
		departmentMapper.updateByPrimaryKeySelective(entity);
	}

	public DepartmentEntity findById(String id) {
		return departmentMapper.selectByPrimaryKey(id);
	}

	public List<Department> findDepartmentList(DepartmentQueryParam param) {
		List<DepartmentEntity> list = departmentMapper.findListByQueryParam(param);
		return list.stream().map(o -> {
			Department department = BeanUtils.copy(o, Department.class);
			return department;
		}).collect(Collectors.toList());
	}

}
