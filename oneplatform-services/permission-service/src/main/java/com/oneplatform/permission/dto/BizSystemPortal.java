package com.oneplatform.system.dto;

import org.apache.commons.lang3.StringUtils;

import com.mendmix.common.GlobalConstants;

public class BizSystemPortal extends BaseDto{

    private String clientType;
    private String indexPath;
	private String tenantId;

	public String getClientType() {
		return clientType;
	}
	public void setClientType(String clientType) {
		this.clientType = clientType;
	}
	public String getIndexPath() {
		return indexPath;
	}
	public void setIndexPath(String indexPath) {
		this.indexPath = indexPath;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getDomain() {
		if(indexPath != null && indexPath.startsWith("http")) {
			return StringUtils.split(indexPath, GlobalConstants.PATH_SEPARATOR)[1];
		}
        return null;
    }

}
