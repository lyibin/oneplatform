package com.oneplatform.system.dto.param;

import java.util.List;

public class UserRoleParam {

    /**
     * id
     */
    private Integer id;

    /**
     * 用户组名称
     */
    private String name;
    
    private String code;
    /**
     * 关联部门id
     */
    private String departmentId;

	private String remarks;

    private List<String> grantResourceIds;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public List<String> getGrantResourceIds() {
		return grantResourceIds;
	}

	public void setGrantResourceIds(List<String> grantResourceIds) {
		this.grantResourceIds = grantResourceIds;
	}

    
}