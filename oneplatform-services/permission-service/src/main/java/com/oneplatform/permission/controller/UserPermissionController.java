package com.oneplatform.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mendmix.common.CurrentRuntimeContext;
import com.mendmix.common.MendmixBaseException;
import com.mendmix.common.annotation.ApiMetadata;
import com.mendmix.common.constants.PermissionLevel;
import com.mendmix.common.model.AuthUser;
import com.mendmix.common.model.DataPermItem;
import com.mendmix.common.model.TreeModel;
import com.mendmix.common.util.BeanUtils;
import com.mendmix.common.util.JsonUtils;
import com.oneplatform.system.constants.FunctionResourceType;
import com.oneplatform.system.constants.RoleType;
import com.oneplatform.system.constants.SubRelationType;
import com.oneplatform.system.dao.entity.ApiResourceEntity;
import com.oneplatform.system.dao.entity.FunctionResourceEntity;
import com.oneplatform.system.dao.entity.UserRoleEntity;
import com.oneplatform.system.dao.mapper.UserRoleEntityMapper;
import com.oneplatform.system.dto.ApiResource;
import com.oneplatform.system.dto.ResourceTreeModel;
import com.oneplatform.system.dto.UserRole;
import com.oneplatform.system.dto.param.AddUserRoleRelationParam;
import com.oneplatform.system.dto.param.FunctionResourceQueryParam;
import com.oneplatform.system.dto.param.ResourceScopeQueryParam;
import com.oneplatform.system.service.FunctionResourceService;
import com.oneplatform.system.service.InternalRelationService;
import com.oneplatform.system.service.UserPermissionService;


@RestController
@RequestMapping("/")
public class UserPermissionController {

    @Autowired
    private InternalRelationService relationService;
    @Autowired
    private UserPermissionService userPermissionService;
    @Autowired
    private FunctionResourceService functionResourceService;
    @Autowired 
    private UserRoleEntityMapper userGroupMapper;
    
    @ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired)
    @PostMapping("user/assign_roles")
    public void updateUserRoleRelation(@RequestBody AddUserRoleRelationParam param) {
    	if(param.getRoleIds() == null || param.getRoleIds().isEmpty()) {
    		throw new MendmixBaseException("至少选择一个角色");
    	}
    	UserRoleEntity userGroup = userGroupMapper.selectByPrimaryKey(param.getRoleIds().get(0));
    	if(userGroup == null)throw new MendmixBaseException("角色不存在");
    	//
    	List<String> strRoleIds = param.getRoleIds().stream().map( o -> {return o.toString();}).collect(Collectors.toList());
    	
    	relationService.updateChildSubRelations(SubRelationType.userToRole, param.getUserId(), strRoleIds);
    }

    
    @ApiMetadata(permissionLevel = PermissionLevel.LoginRequired)
    @GetMapping(value = "/user/roles")
    public List<UserRole> getUserRoles() {
    	ResourceScopeQueryParam current = ResourceScopeQueryParam.current();
        List<UserRoleEntity> entities = userPermissionService.findUserAssignRoles(RoleType.function, current.getUserId(), current.getDepartmentId());
        return BeanUtils.copy(entities, UserRole.class);
    }

    
    @ApiMetadata(permissionLevel = PermissionLevel.LoginRequired)
    @GetMapping(value = "/user/apis")
    public List<ApiResource> getUserApiPermissions() {
    	ResourceScopeQueryParam current = ResourceScopeQueryParam.current();
        List<ApiResourceEntity> entities = userPermissionService.findUserGrantedApis(current);
        return BeanUtils.copy(entities, ApiResource.class);
    }

    
    @ApiMetadata(permissionLevel = PermissionLevel.LoginRequired)
    @GetMapping(value = "/user/buttons")
    public List<String> getUserButtonPermissions() {
    	
    	AuthUser currentUser = CurrentRuntimeContext.getAndValidateCurrentUser();
    	List<FunctionResourceEntity> entities;
    	if(currentUser.isAdmin()) {
    		//TODO 查找默认管理员 判断多租户
    		FunctionResourceQueryParam queryParam = new FunctionResourceQueryParam();
    		queryParam.setType(FunctionResourceType.button.name());
    		queryParam.setClientType(CurrentRuntimeContext.getClientType());
    		queryParam.setIncludeHidden(true);
    		queryParam.setEnabled(true);
    		entities = functionResourceService.findByQueryParam(queryParam);
    	}else {
    		ResourceScopeQueryParam current = ResourceScopeQueryParam.current();
        	current.setType(FunctionResourceType.button.name());
        	entities = userPermissionService.findUserGrantedResources(current);
    	}
    	return entities.stream().map(o -> o.getCode()).collect(Collectors.toList());
    }
    
    @ApiMetadata(permissionLevel = PermissionLevel.LoginRequired)
    @GetMapping(value = "/user/menus")
    public List<TreeModel> getUserMenuPermissions(){
    	
    	String clientType = CurrentRuntimeContext.getClientType();
    	AuthUser currentUser = CurrentRuntimeContext.getAndValidateCurrentUser();
    	List<FunctionResourceEntity> entities;
    	if(currentUser.isAdmin()) {
    		//TODO 查找默认管理员 判断多租户
    		FunctionResourceQueryParam queryParam = new FunctionResourceQueryParam();
    		queryParam.setExcludeButton(true);
    		queryParam.setIncludeHidden(true);
			queryParam.setClientType(clientType);
    		queryParam.setEnabled(true);
    		entities = functionResourceService.findByQueryParam(queryParam);
    	}else {
    		ResourceScopeQueryParam current = ResourceScopeQueryParam.current();
        	current.setType(FunctionResourceType.menu.name());
        	entities = userPermissionService.findUserGrantedResources(current);
    	}
    	
    	List<ResourceTreeModel> menus = new ArrayList<>(entities.size());
    	ResourceTreeModel model;
        for (FunctionResourceEntity entity : entities) {
        	model = BeanUtils.copy(entity, ResourceTreeModel.class);
        	menus.add(model);
		}
    	
    	return TreeModel.build(menus).getChildren();
    }

    @ApiMetadata(permissionLevel = PermissionLevel.LoginRequired)
    @GetMapping(value = "/user/permissions")
    public Map<String, Object> getUserPermissions(){
    	Map<String, Object> result = new HashMap<>(2);
    	result.put("menus", getUserMenuPermissions());
    	result.put("buttons", getUserButtonPermissions());
    	return result;
    }
    
    @ApiMetadata(permissionLevel = PermissionLevel.LoginRequired)
    @GetMapping(value = "/user/data_roles")
    public List<UserRole> getUserDataRoles() {
    	ResourceScopeQueryParam current = ResourceScopeQueryParam.current();
        List<UserRoleEntity> entities = userPermissionService.findUserAssignRoles(RoleType.data, current.getUserId(), current.getDepartmentId());
        return BeanUtils.copy(entities, UserRole.class);
    }
    
    @ApiMetadata(permissionLevel = PermissionLevel.LoginRequired)
    @GetMapping(value = "/user/data_permissions")
    public Map<String, DataPermItem> getUserDataPerms() {
    	ResourceScopeQueryParam current = ResourceScopeQueryParam.current();
    	Map<String, DataPermItem> result = new HashMap<>();
		
        List<UserRoleEntity> entities = userPermissionService.findUserAssignRoles(RoleType.data, current.getUserId(), current.getDepartmentId());
        
        List<DataPermItem> items;
        for (UserRoleEntity entity : entities) {
        	items = JsonUtils.toList(entity.getPermissions(), DataPermItem.class);
        	for (DataPermItem item : items) {
				if(!result.containsKey(item.getKey())) {
					result.put(item.getKey(), item);
				}else if(!item.getValues().isEmpty()){
					for (String val : item.getValues()) {
						if(!result.get(item.getKey()).getValues().contains(val)) {
							result.get(item.getKey()).getValues().add(val);
						}
					}
				}
			}
		}
        
        return result;
    }
   
}
