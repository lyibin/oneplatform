import config from "@/config"
import http from "@/utils/request"

export default {
	accountLogin: {
		url: `${config.API_URL}/login`,
		name: "登录获取TOKEN",
		post: async function(data={}){
			return await http.post(this.url, data);
		}
	},
	userPermissions: {
		url: `${config.API_URL}/sys/user/permissions`,
		name: "获取我的菜单",
		get: async function() {
			let res = await http.get(this.url);
			res.data.menus = convertToMenuItems(res.data.menus);
			return res;
		}
	}
}
