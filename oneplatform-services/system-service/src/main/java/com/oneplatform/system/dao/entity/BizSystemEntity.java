package com.oneplatform.system.dao.entity;

import javax.persistence.Column;
import javax.persistence.Table;

import com.oneplatform.system.dao.StandardBaseEntity;

@Table(name = "biz_system")
public class BizSystemEntity extends StandardBaseEntity {

    private String name;

    @Column(name = "code",updatable = false)
    private String code;

    private String category;

    @Column(name = "api_baseurl")
    private String apiBaseurl;

    @Column(name = "master_uid")
    private String masterUid;

    @Column(name = "master_uname")
    private String masterUname;

    @Column(name = "master_phone")
    private String masterPhone;

    private String secret;

    /**
     * 是否内部系统
     */
    private Boolean internal;


    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return api_baseurl
     */
    public String getApiBaseurl() {
        return apiBaseurl;
    }

    /**
     * @param apiBaseurl
     */
    public void setApiBaseurl(String apiBaseurl) {
        this.apiBaseurl = apiBaseurl;
    }

    /**
     * @return master_uid
     */
    public String getMasterUid() {
        return masterUid;
    }

    /**
     * @param masterUid
     */
    public void setMasterUid(String masterUid) {
        this.masterUid = masterUid;
    }

    /**
     * @return master_uname
     */
    public String getMasterUname() {
        return masterUname;
    }

    /**
     * @param masterUname
     */
    public void setMasterUname(String masterUname) {
        this.masterUname = masterUname;
    }

    /**
     * @return master_phone
     */
    public String getMasterPhone() {
        return masterPhone;
    }

    /**
     * @param masterPhone
     */
    public void setMasterPhone(String masterPhone) {
        this.masterPhone = masterPhone;
    }

    /**
     * @return secret
     */
    public String getSecret() {
        return secret;
    }

    /**
     * @param secret
     */
    public void setSecret(String secret) {
        this.secret = secret;
    }

    /**
     * 获取是否内部系统
     *
     * @return internal - 是否内部系统
     */
    public Boolean getInternal() {
        return internal;
    }

    /**
     * 设置是否内部系统
     *
     * @param internal 是否内部系统
     */
    public void setInternal(Boolean internal) {
        this.internal = internal;
    }

}