package com.oneplatform.system.dto.param;

import java.util.List;

/**
 * 
 * <br>
 * Class Name   : GrantUserRolePermParam
 *
 * @author jiangwei
 * @version 1.0.0
 * @date 2019年9月29日
 */
public class GrantUserRolePermParam {

	private Integer roleId;
	private List<GrantPermItem> permItems;
	
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public List<GrantPermItem> getPermItems() {
		return permItems;
	}
	public void setPermItems(List<GrantPermItem> permItems) {
		this.permItems = permItems;
	}
	
	
 }
