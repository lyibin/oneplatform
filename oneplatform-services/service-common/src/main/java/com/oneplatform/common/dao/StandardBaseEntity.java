package com.oneplatform.common.dao;

import javax.persistence.Id;

public abstract class StandardBaseEntity extends OperateBaseEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
    private String id;
	
	private Boolean enabled;
    private Boolean deleted;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	
	

}
