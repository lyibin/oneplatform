package com.oneplatform.system.dao.entity;

import javax.persistence.Column;
import javax.persistence.Table;

import com.oneplatform.system.dao.StandardBaseEntity;

@Table(name = "dashboard_widget")
public class DashboardWidgetEntity extends StandardBaseEntity {
 
	private static final long serialVersionUID = 1L;

	private String name;

    private String code;

    @Column(name = "source_type")
    private String sourceType;

    @Column(name = "source_url")
    private String sourceUrl;

    @Column(name = "layout_type")
    private String layoutType;

    @Column(name = "system_id")
    private String systemId;

    /**
     * 是否开放访问
     */
    @Column(name = "is_open_access")
    private Boolean isOpenAccess;

    private String content;

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return source_type
     */
    public String getSourceType() {
        return sourceType;
    }

    /**
     * @param sourceType
     */
    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    /**
     * @return source_url
     */
    public String getSourceUrl() {
        return sourceUrl;
    }

    /**
     * @param sourceUrl
     */
    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    /**
     * @return layout_type
     */
    public String getLayoutType() {
        return layoutType;
    }

    /**
     * @param layoutType
     */
    public void setLayoutType(String layoutType) {
        this.layoutType = layoutType;
    }

    /**
     * @return system_id
     */
    public String getSystemId() {
        return systemId;
    }

    /**
     * @param systemId
     */
    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

    /**
     * 获取是否开放访问
     *
     * @return is_open_access - 是否开放访问
     */
    public Boolean getIsOpenAccess() {
        return isOpenAccess;
    }

    /**
     * 设置是否开放访问
     *
     * @param isOpenAccess 是否开放访问
     */
    public void setIsOpenAccess(Boolean isOpenAccess) {
        this.isOpenAccess = isOpenAccess;
    }

    /**
     * @return content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content
     */
    public void setContent(String content) {
        this.content = content;
    }
}