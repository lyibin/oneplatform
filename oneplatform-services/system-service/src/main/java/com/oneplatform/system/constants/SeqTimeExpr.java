/**
 * Confidential and Proprietary Copyright 2019 By 卓越里程教育科技有限公司 All Rights Reserved
 */
package com.oneplatform.system.constants;

/**
 * 
 * <br>
 * Class Name   : SeqExpr
 *
 * @author jiangwei
 * @version 1.0.0
 * @date 2019年10月8日
 */
public enum SeqTimeExpr {
	
	YEAR("{yyyy}"),MONTH("{MM}"),DAY("{dd}");

	private final String expr;

	/**
	 * @param expr
	 */
	private SeqTimeExpr(String expr) {
		this.expr = expr;
	}

	/**
	 * @return the expr
	 */
	public String getExpr() {
		return expr;
	}
	
	
	
}
