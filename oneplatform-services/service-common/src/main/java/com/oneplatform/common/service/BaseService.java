/*
 * Copyright 2016-2022 www.mendmix.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.oneplatform.common.service;

import java.io.Serializable;
import java.util.Objects;

import com.mendmix.common.MendmixBaseException;
import com.mendmix.common.exception.MainErrorType;
import com.mendmix.common.model.Page;
import com.mendmix.common.model.PageParams;
import com.mendmix.common.util.BeanUtils;
import com.mendmix.mybatis.core.BaseEntity;
import com.mendmix.mybatis.core.BaseMapper;
import com.mendmix.mybatis.plugin.pagination.PageExecutor;
import com.oneplatform.common.dao.StandardBaseEntity;

/**
 * @description <br>
 * @author <a href="mailto:vakinge@gmail.com">vakinge</a>
 * @date Jun 22, 2023
 */
public abstract class BaseService <E extends BaseEntity,ID extends Serializable,M extends BaseMapper<E, ID>>{

	protected M mapper;
	
	public E findById(ID id) {
		return mapper.selectByPrimaryKey(id);
	}
	
	public E add(E entity) {
		if(findUniqueOne(entity) != null) {
			throw new MendmixBaseException(MainErrorType.OBJECT_EXISTS);
		}
		mapper.insertSelective(entity);
		return entity;
	}
	
	public void updateNoneCheck(E entity) {
		mapper.updateByPrimaryKey(entity);
	}
	
	public E updateById(E entity) {
		E uniqueEntity = findUniqueOne(entity);
		E originEntity = null;
		if(uniqueEntity != null) {
			if(!Objects.equals(entity.getId(), uniqueEntity.getId())) {
				throw new MendmixBaseException(MainErrorType.OBJECT_EXISTS);
			}
			originEntity = uniqueEntity;
		}
		if(originEntity == null) {
			originEntity = mapper.selectByPrimaryKey((ID)entity.getId());
		}
		if(originEntity == null) {
			throw new MendmixBaseException(MainErrorType.OBJECT_NOT_EXISTS);
		}
		BeanUtils.copy(entity, originEntity);
		mapper.updateByPrimaryKey(originEntity);
		return entity;
	}
	
	public void toggleById(ID id) {
		E entity = mapper.selectByPrimaryKey(id);
		if(entity instanceof StandardBaseEntity) {
			final StandardBaseEntity se = (StandardBaseEntity)entity;
			se.setEnabled(!se.getEnabled());
			mapper.updateByPrimaryKeySelective(entity);
		}
	}
	
	public void deleteById(ID id) {
		E entity = mapper.selectByPrimaryKey(id);
		if(entity instanceof StandardBaseEntity) {
			((StandardBaseEntity)entity).setDeleted(true);
			mapper.updateByPrimaryKeySelective(entity);
		}else {
			mapper.deleteByPrimaryKey(id);
		}
	}
	
	public Page<E> pageQuery(PageParams pageParam,Object example){
		return PageExecutor.pagination(pageParam, () -> mapper.selectByExample(example));
	}
	
	protected abstract E findUniqueOne(E entity);

}
