package com.oneplatform.system.dao.entity;

import javax.persistence.Column;
import javax.persistence.Table;

import com.oneplatform.system.dao.StandardBaseEntity;

@Table(name = "sequence_rules")
public class SequenceRuleEntity extends StandardBaseEntity {
	
	private static final long serialVersionUID = 1L;

    /**
    * @return name 名称
    */
    @Column(name = "name")
    private String name;
    
    /**
    * @return code 序列业务编号
    */
    @Column(name = "code",updatable = false)
    private String code;
    
    @Column(name = "system_id",updatable = false)
    private Integer systemId;
    
    /**
    * @return prefix 前缀
    */
    @Column(name = "prefix")
    private String prefix;
    
    /**
    * @return expression 序列表达式
    */
    @Column(name = "time_expr")
    private String timeExpr;
    
    /**
    * @return seqLength 自增序列位数
    */
    @Column(name = "seq_length")
    private Integer seqLength;
    
    @Column(name = "random_type")
    private String randomType;
    
    @Column(name = "random_length")
    private Integer randomLength;
    
    /**
    * @return firstSequence 初始自增序列值
    */
    @Column(name = "first_sequence",updatable = false)
    private Integer firstSequence = 1;
    
    /**
    * @return lastSequence 最新自增序列值
    */
    @Column(name = "last_sequence")
    private Integer lastSequence;
    
    private String memo;
   
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    

	public Integer getSystemId() {
		return systemId;
	}

	public void setSystemId(Integer systemId) {
		this.systemId = systemId;
	}

	public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
    
    /**
	 * @return the timeExpr
	 */
	public String getTimeExpr() {
		return timeExpr;
	}

	/**
	 * @param timeExpr the timeExpr to set
	 */
	public void setTimeExpr(String timeExpr) {
		this.timeExpr = timeExpr;
	}

	public Integer getSeqLength() {
        return seqLength;
    }

    public void setSeqLength(Integer seqLength) {
        this.seqLength = seqLength;
    }

    
    /**
	 * @return the randomType
	 */
	public String getRandomType() {
		return randomType;
	}

	/**
	 * @param randomType the randomType to set
	 */
	public void setRandomType(String randomType) {
		this.randomType = randomType;
	}

	/**
	 * @return the randomLength
	 */
	public Integer getRandomLength() {
		return randomLength;
	}

	/**
	 * @param randomLength the randomLength to set
	 */
	public void setRandomLength(Integer randomLength) {
		this.randomLength = randomLength;
	}

	public Integer getFirstSequence() {
        return firstSequence;
    }

    public void setFirstSequence(Integer firstSequence) {
        this.firstSequence = firstSequence;
    }
    public Integer getLastSequence() {
        return lastSequence == null ? firstSequence : lastSequence;
    }

    public void setLastSequence(Integer lastSequence) {
        this.lastSequence = lastSequence;
    }

	/**
	 * @return the memo
	 */
	public String getMemo() {
		return memo;
	}

	/**
	 * @param memo the memo to set
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}
    
    
 }