package com.oneplatform.system.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mendmix.common.annotation.ApiMetadata;
import com.mendmix.common.constants.PermissionLevel;
import com.mendmix.common.model.IdParam;
import com.mendmix.common.model.Page;
import com.mendmix.common.model.PageParams;
import com.mendmix.common.model.PageQueryRequest;
import com.mendmix.common.model.SelectOption;
import com.mendmix.common.util.BeanUtils;
import com.oneplatform.system.constants.SeqTimeExpr;
import com.oneplatform.system.dao.entity.SequenceRuleEntity;
import com.oneplatform.system.dto.SequenceRule;
import com.oneplatform.system.dto.SequenceRuleParam;
import com.oneplatform.system.service.SequenceRuleService;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * <br>
 * Class Name   : SequenceRuleController
 *
 */
@RestController
@RequestMapping("/sequence/rule")
public class SequenceRuleController {

	private @Autowired SequenceRuleService sequenceRuleService;
	
	private static List<SelectOption> timeExprOptions = new ArrayList<SelectOption>();
	static{
		for (SeqTimeExpr seqTimeExpr : SeqTimeExpr.values()) {
			timeExprOptions.add(new SelectOption(seqTimeExpr.getExpr(), seqTimeExpr.name()));
		}
	}
	
	@ApiMetadata(permissionLevel = PermissionLevel.Anonymous)
	@ApiOperation(value = "时间表达式选项")
	@GetMapping("timeExprs")
	public @ResponseBody List<SelectOption> timeExprOptions(){
		return timeExprOptions;
	}

	@ApiMetadata(permissionLevel = PermissionLevel.LoginRequired)
	@ApiOperation(value = "分页查询", notes = "### 分页查询规则信息 \n - xxx")
	@PostMapping("list")
	public @ResponseBody Page<SequenceRule> pageQuery(@RequestBody PageQueryRequest<Void> param) {
		Page<SequenceRule> page = sequenceRuleService.pageQuery(new PageParams(param.getPageNo(), param.getPageSize()));
		return page;
	}

	@ApiMetadata(permissionLevel = PermissionLevel.LoginRequired)
	@ApiOperation(value = "按id查询序列规则")
	@GetMapping("{id}")
	public @ResponseBody
	SequenceRule findById(@PathVariable("id") Integer id) {
		SequenceRuleEntity entity = sequenceRuleService.findRule(id);
		return BeanUtils.copy(entity, SequenceRule.class);
	}

	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired)
	@ApiOperation(value = "新增序列规则")
	@PostMapping("add")
	public @ResponseBody
	IdParam<Integer> addRegulation(@RequestBody @Validated SequenceRuleParam param) {
		param.setId(null);
		Integer id = sequenceRuleService.addRule(param);
		return new IdParam<>(id);
	}

	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired)
	@ApiOperation(value = "更新序列规则")
	@PostMapping("update")
	public @ResponseBody
	void updateRegulation(@RequestBody @Validated SequenceRuleParam param) {
		sequenceRuleService.updateRule(param);
	}

	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired)
	@ApiOperation(value = "启用/禁用")
	@PostMapping("toggle/{id}")
	public @ResponseBody
	void toggleRegulation(@PathVariable(value = "id") Integer id) {
		sequenceRuleService.enableSwitch(id);
	}

}
