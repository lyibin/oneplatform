package com.oneplatform.controller;

import java.io.IOException;
import java.io.SequenceInputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mendmix.common.GlobalConstants;
import com.mendmix.common.annotation.ApiMetadata;
import com.mendmix.common.constants.PermissionLevel;
import com.mendmix.common.model.WrapperResponse;
import com.mendmix.common.util.MimeTypeUtils;
import com.mendmix.cos.CUploadObject;
import com.mendmix.cos.CUploadResult;
import com.mendmix.cos.CosProviderServiceFacade;
import com.mendmix.cos.FilePathHelper;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class CommonController {

	@Autowired(required = false)
	private CosProviderServiceFacade cosProviderServiceFacade;
	
	@PostMapping("file/upload")
    @ResponseBody
    @ApiMetadata(actionName = "上传文件",permissionLevel = PermissionLevel.Anonymous,actionLog = false)
	public Mono<WrapperResponse<CUploadResult>> upload(ServerHttpRequest request,@RequestPart("file") FilePart filePart) {
		
		String fileName = filePart.filename();
		Flux<DataBuffer> bufferFlux = filePart.content();
		Mono<WrapperResponse<CUploadResult>> mono;
		mono = bufferFlux.map(dataBuffer -> dataBuffer.asInputStream()) //
				.reduce(SequenceInputStream::new) //
				.flatMap(inputStream -> {
					try {
						String mimeType = null;
						MediaType contentType = filePart.headers().getContentType();
						if(contentType != null) {
							mimeType = contentType.toString();
						} else {
							String suffix = FilePathHelper.getSuffix(fileName);
							mimeType = MimeTypeUtils.getFileMimeType(suffix);
						}
						byte[] bytes = IOUtils.toByteArray(inputStream);
						CUploadResult result = cosProviderServiceFacade.upload(new CUploadObject(bytes, mimeType));
						return Mono.just(new WrapperResponse<>(result));
					} catch (IOException e) {
						e.printStackTrace();
						return Mono.just(new WrapperResponse<>(500,"上传失败"));
					}
				});
		return mono;
	}
	
    @GetMapping("/file/{fileKey}")
    public Mono<Void> downloadFile(@PathVariable("fileKey") String fileKey,ServerHttpResponse response) throws IOException {
      
    	String suffix = FilePathHelper.getSuffix(fileKey);
    	String mimeType = MimeTypeUtils.getFileMimeType(suffix);
    	String fileName = fileKey;
    	if(fileKey.contains(GlobalConstants.PATH_SEPARATOR)) {
    		fileName = fileName.substring(fileName.lastIndexOf(GlobalConstants.PATH_SEPARATOR) + 1);
    	}
        response.getHeaders().set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename="+fileName);
        MediaType mediaType = MediaType.parseMediaType(mimeType);
        response.getHeaders().setContentType(mediaType);

        byte[] bytes = cosProviderServiceFacade.getObjectBytes(null, fileKey);
        return response.writeWith(Mono.just(response.bufferFactory().wrap(bytes)));
    }
}
