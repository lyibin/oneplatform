package com.oneplatform.system.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mendmix.common.annotation.ApiMetadata;
import com.mendmix.common.constants.PermissionLevel;
import com.mendmix.common.model.IdParam;
import com.mendmix.common.model.Page;
import com.mendmix.common.model.PageQueryRequest;
import com.mendmix.common.model.SelectOption;
import com.mendmix.common.util.BeanUtils;
import com.oneplatform.system.dao.entity.BizSystemModuleEntity;
import com.oneplatform.system.dto.BizSystemModule;
import com.oneplatform.system.dto.param.SystemModuleParam;
import com.oneplatform.system.service.BizSystemService;


@RestController
@RequestMapping("/system/module")
public class SystemModuleController {

    private @Autowired BizSystemService systemService;

    
    @ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired,actionLog = true)
    @PostMapping(value = "add")
    @ResponseBody
    public IdParam<Integer> add(@RequestBody SystemModuleParam param) {
    	BizSystemModuleEntity entity = BeanUtils.copy(param, BizSystemModuleEntity.class);
        Integer id = systemService.addSystemModule(entity);
        return new IdParam<Integer>(id);
    }

    
    @ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired,actionLog = true)
    @PostMapping(value = "delete")
    @ResponseBody
    public void delete(@RequestBody IdParam<Integer> param) {
    	BizSystemModuleEntity entity = systemService.findSystemModuleById(param.getId());
    	entity.setDeleted(true);
    	systemService.updateSystemModule(entity);
    }

    
    @ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired,actionLog = true)
    @PostMapping("update")
    @ResponseBody
    public void update(@RequestBody SystemModuleParam param) {
    	BizSystemModuleEntity entity = BeanUtils.copy(param, BizSystemModuleEntity.class);
    	systemService.updateSystemModule(entity);
    }

    
    @ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired,actionLog = true)
    @PostMapping("toggle")
    @ResponseBody
    public void toggleApi(@RequestBody IdParam<Integer> param) {
    	BizSystemModuleEntity entity = systemService.findSystemModuleById(param.getId());
    	entity.setEnabled(!entity.getEnabled());
    	systemService.updateSystemModule(entity);
    }

    
    @ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired,actionLog = true)
    @GetMapping(value = "{id}")
    @ResponseBody
    public BizSystemModule get(Integer id) {
    	BizSystemModuleEntity entity = systemService.findSystemModuleById(id);
        return BeanUtils.copy(entity, BizSystemModule.class);
    }

    
    @ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired,actionLog = true)
    @PostMapping("list")
    @ResponseBody
    public Page<BizSystemModule> pageQry(@RequestBody PageQueryRequest<Void> queryParam) {
        return systemService.pageQuerySystemModules(queryParam.asPageParam());
    }

    
    @GetMapping("options")
    @ResponseBody
    public List<SelectOption> options() {
    	List<BizSystemModuleEntity> list = systemService.getSystemModules();
    	return list.stream().map(e -> {
            return new SelectOption(e.getId().toString(),e.getName());
        }).collect(Collectors.toList());
    }

    
}
