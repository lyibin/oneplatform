## 简介

基于[Naive Ui Admin](https://github.com/jekip/naive-ui-admin)[文档](https://naive-ui-admin-docs.vercel.app/) 和 [fast-crud](https://github.com/fast-crud/fast-crud)[文档](http://fast-crud.docmirror.cn/) 构建。


## 使用


- 安装依赖

```bash
yarn install

```

- 运行

```bash
yarn dev
```

- 打包

```bash
yarn build
```
