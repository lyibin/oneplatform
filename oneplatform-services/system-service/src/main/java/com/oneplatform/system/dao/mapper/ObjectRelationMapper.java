package com.oneplatform.system.dao.mapper;

import java.util.List;

import com.oneplatform.system.dto.ObjectRelation;

public interface ObjectRelationMapper {
	
	int insertGrantRelation(List<ObjectRelation> relations);
	
	int insertBindingRelation(List<ObjectRelation> relations);
	
	int insertSubordinateRelation(List<ObjectRelation> relations);
	
	int deleteGrantRelation(ObjectRelation relation);
	
	int deleteBindingRelation(ObjectRelation relation);
	
	int deleteSubordinateRelation(ObjectRelation relation);
	
	List<ObjectRelation> findGrantRelations(ObjectRelation example);
	
	List<ObjectRelation> findBindingRelations(ObjectRelation example);
	
	List<ObjectRelation> findSubordinateRelations(ObjectRelation example);

}