package com.oneplatform.system.dto;

import com.mendmix.common.CurrentRuntimeContext;

/**
 * 
 * 
 * <br>
 * Class Name   : ObjectRelation
 *
 * @author <a href="mailto:vakinge@gmail.com">vakin</a>
 * @version 1.0.0
 * @date Apr 19, 2022
 */
public class ObjectRelation {

    private String firstId;
    private String firstName;
    private String secondId;
    private String secondName;
    private String relationType;
    private String systemId;

	public ObjectRelation() {
		this.systemId = CurrentRuntimeContext.getSystemId();
	}
	
	public ObjectRelation(String relationType, String firstId, String secondId) {
		this.relationType = relationType;
		this.firstId = firstId;
		this.secondId = secondId;
		this.systemId = CurrentRuntimeContext.getSystemId();
	}

	public String getFirstId() {
		return firstId;
	}

	public void setFirstId(String firstId) {
		this.firstId = firstId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSecondId() {
		return secondId;
	}

	public void setSecondId(String secondId) {
		this.secondId = secondId;
	}


	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getRelationType() {
		return relationType;
	}

	public void setRelationType(String relationType) {
		this.relationType = relationType;
	}

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

    
    
}