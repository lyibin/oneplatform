package com.oneplatform.common.dao;

import java.util.Date;

import javax.persistence.Column;

import org.apache.commons.lang3.StringUtils;

import com.mendmix.mybatis.core.BaseEntity;
import com.mendmix.mybatis.plugin.autofield.annotation.CreatedAt;
import com.mendmix.mybatis.plugin.autofield.annotation.CreatedBy;
import com.mendmix.mybatis.plugin.autofield.annotation.UpdatedAt;
import com.mendmix.mybatis.plugin.autofield.annotation.UpdatedBy;

public abstract class OperateBaseEntity extends BaseEntity {

	private static final long serialVersionUID = 1L;


    @CreatedAt
    @Column(name = "created_at")
    private Date createdAt;

    @CreatedBy
    @Column(name = "created_by")
    private String createdBy;

    /**
     * 更新时间
     */
    @UpdatedAt
    @Column(name = "updated_at")
    private Date updatedAt;

    @UpdatedBy
    @Column(name = "updated_by")
    private String updatedBy;

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		if(updatedAt == null)return createdAt;
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return StringUtils.defaultString(updatedBy, createdBy);
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	

}
