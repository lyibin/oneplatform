package com.oneplatform.system.dao.mapper;

import com.mendmix.mybatis.core.BaseMapper;
import com.oneplatform.system.dao.entity.DashboardWidgetEntity;

public interface DashboardWidgetEntityMapper extends BaseMapper<DashboardWidgetEntity, Integer> {
}