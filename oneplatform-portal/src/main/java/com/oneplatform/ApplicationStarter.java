package com.oneplatform;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mendmix.gateway.task.SystemMateRefreshTask;
import com.mendmix.logging.actionlog.storage.HttpApiLogStorageProvider;
import com.mendmix.springcloud.starter.BaseApplicationStarter;


@SpringBootApplication
@EnableDiscoveryClient
@EnableTransactionManagement
@ComponentScan(value = {"com.oneplatform","com.mendmix.springcloud.autoconfigure","com.mendmix.gateway.autoconfigure"})
public class ApplicationStarter extends BaseApplicationStarter{

	public static void main(String[] args) {
		long starTime = before(args);
		new SpringApplicationBuilder(ApplicationStarter.class).web(WebApplicationType.REACTIVE).run(args);
		after(starTime);
	}
	
	//@Bean
	public SystemMateRefreshTask systemMateRefreshTask() {
		return new SystemMateRefreshTask();
	}
	
	@Bean
    public HttpApiLogStorageProvider httpApiLogStorageProvider() {
		return new HttpApiLogStorageProvider();
	}
	
}
