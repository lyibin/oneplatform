package com.oneplatform.system.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mendmix.scheduler.AbstractJob;
import com.mendmix.scheduler.JobContext;
import com.mendmix.scheduler.annotation.ScheduleConf;
import com.oneplatform.system.dao.mapper.ActionLogEntityMapper;

@Service
@ScheduleConf(cronExpr = "0/10 * * * * ?")
public class SystemAlarmTask extends AbstractJob {

	@Autowired
	private ActionLogEntityMapper actionLogMapper;
	

	@Override
	public void doJob(JobContext context) throws Exception {
		//System.out.println("===SystemAlarmTask===");
	}


	@Override
	public boolean logging() {
		return false;
	}
	
	

}
