package com.oneplatform.support.auth;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.mendmix.common.MendmixBaseException;
import com.mendmix.common.http.HttpRequestEntity;
import com.mendmix.common.model.AuthUser;
import com.mendmix.common.util.BeanUtils;
import com.mendmix.gateway.GatewayConfigs;
import com.mendmix.gateway.api.AccountApi;
import com.mendmix.gateway.security.GatewaySecurityDecisionProvider;
import com.mendmix.security.model.ApiPermission;

@Component
public class OnePlatformSecurityDecisionProvider extends GatewaySecurityDecisionProvider {

	@Value("${permission.service.baseUrl:http://oneplatform-permission-svc}/user/apis")
	private String userApiUrl;
	
	@Autowired
	private AccountApi accountService;

	@Override
	public AuthUser validateUser(String name, String password) throws MendmixBaseException {
		
		AuthUser authUser = accountService.validateAccount(name, password);

		LoginUserInfo userInfo = BeanUtils.copy(authUser, LoginUserInfo.class);
		
		return userInfo;
	}


	@Override
	public boolean apiAuthzEnabled() {
		return false;
	}



	@Override
	public List<ApiPermission> getUserApiPermissions(String userId) {
		List<ApiPermission> list = HttpRequestEntity.get(userApiUrl).useContext().execute().toList(ApiPermission.class);
		if(list != null) {
			for (ApiPermission api : list) {
				api.setUri(GatewayConfigs.PATH_PREFIX + api.getUri());
			}
		}
		return list;
	}

	@Override
	public String error401Page() {
		return null;
	}

	@Override
	public String error403Page() {
		return null;
	}

}
