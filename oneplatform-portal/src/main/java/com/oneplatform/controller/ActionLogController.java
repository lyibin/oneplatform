package com.oneplatform.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mendmix.common.annotation.ApiMetadata;
import com.mendmix.common.constants.PermissionLevel;
import com.mendmix.common.model.Page;
import com.mendmix.common.model.PageQueryRequest;
import com.mendmix.common.model.WrapperResponse;
import com.mendmix.logging.actionlog.ActionLog;
import com.mendmix.logging.actionlog.ActionLogQueryParam;
import com.mendmix.logging.actionlog.LogStorageProvider;

@RestController
@RequestMapping("/actionlog")
public class ActionLogController {

	@Autowired
	private LogStorageProvider logStorageProvider;

	@ApiMetadata(permissionLevel = PermissionLevel.LoginRequired,actionLog = false)
	@PostMapping("list")
	public WrapperResponse<Page<ActionLog>> pageQuery(@RequestBody PageQueryRequest<ActionLogQueryParam> param) {
		Page<ActionLog> page = logStorageProvider.pageQuery(param.asPageParam(), param.getExample());
		return WrapperResponse.success(page);
	}
	
	@ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired,actionLog = false)
	@GetMapping("details")
	public WrapperResponse<ActionLog> getDetailsById(@RequestParam("id") String id) {
		ActionLog details = logStorageProvider.getDetails(id);
		return WrapperResponse.success(details);
	}
}
