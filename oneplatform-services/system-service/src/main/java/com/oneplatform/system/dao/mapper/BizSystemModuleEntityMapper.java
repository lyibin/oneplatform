package com.oneplatform.system.dao.mapper;

import com.mendmix.mybatis.core.BaseMapper;
import com.oneplatform.system.dao.entity.BizSystemModuleEntity;

public interface BizSystemModuleEntityMapper extends BaseMapper<BizSystemModuleEntity, Integer> {

}