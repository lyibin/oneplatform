package com.oneplatform.user.dto;

public class UserScope {

	private String id;
	private String tenantId;
	private String platformType;
	private String principalId;
	private boolean admin;
	

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	
	public String getSystemId() {
		return platformType;
	}
	public void setSystemId(String platformType) {
		this.platformType = platformType;
	}
	public String getPrincipalId() {
		return principalId;
	}
	public void setPrincipalId(String principalId) {
		this.principalId = principalId;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

}
