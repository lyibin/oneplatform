package com.oneplatform.user.dao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.mendmix.mybatis.core.BaseMapper;
import com.mendmix.mybatis.plugin.cache.annotation.Cache;
import com.oneplatform.user.dao.entity.UserScopeEntity;

public interface UserScopeEntityMapper extends BaseMapper<UserScopeEntity, String> {
	@Cache
	@Select("SELECT * FROM user_scope WHERE user_id=#{userId} AND enabled=1")
	@ResultMap("BaseResultMap")
	List<UserScopeEntity> findByUserId(String userId);
}