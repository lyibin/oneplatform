package com.oneplatform.user.dao.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mendmix.mybatis.core.BaseEntity;
import com.mendmix.mybatis.plugin.autofield.annotation.CreatedAt;
import com.mendmix.mybatis.plugin.autofield.annotation.CreatedBy;

@Table(name = "user_wallet_log")
public class UserWalletLogEntity extends BaseEntity {
    @Id
    private String id;

    @Column(name = "user_id")
    private String userId;
    
    @Column(name = "tenant_id")
    private String tenantId;

    @Column(name = "wallet_id")
    private String walletId;

    @Column(name = "order_no")
    private String orderNo;

    @Column(name = "trade_name")
    private String tradeName;

    /**
     * 余额，平台币，积分
     */
    @Column(name = "wallet_type")
    private String walletType;

    /**
     * 可用的，冻结的
     */
    @Column(name = "sub_type")
    private String subType;

    /**
     * 支出，收入，冻结，解冻
     */
    @Column(name = "trade_type")
    private String tradeType;

    private BigDecimal amount;

    /**
     * 当前可用的
     */
    @Column(name = "current_available")
    private BigDecimal currentAvailable;

    /**
     * 当前冻结的
     */
    @Column(name = "current_frozen")
    private BigDecimal currentFrozen;

    private String sign;

    private String memo;

    /**
     * 是否隐藏(不对外显示)
     */
    private Boolean hidden;

    @Column(name = "trade_at")
    private Date tradeAt;

    @Column(name = "created_at")
    @CreatedAt
    private Date createdAt;

    @Column(name = "created_by")
    @CreatedBy
    private String createdBy;

    /**
     * @return id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return user_id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    
    public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	/**
     * @return wallet_id
     */
    public String getWalletId() {
        return walletId;
    }

    /**
     * @param walletId
     */
    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    /**
     * @return order_no
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * @param orderNo
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * @return trade_name
     */
    public String getTradeName() {
        return tradeName;
    }

    /**
     * @param tradeName
     */
    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    /**
     * 获取余额，平台币，积分
     *
     * @return wallet_type - 余额，平台币，积分
     */
    public String getWalletType() {
        return walletType;
    }

    /**
     * 设置余额，平台币，积分
     *
     * @param walletType 余额，平台币，积分
     */
    public void setWalletType(String walletType) {
        this.walletType = walletType;
    }

    /**
     * 获取可用的，冻结的
     *
     * @return sub_type - 可用的，冻结的
     */
    public String getSubType() {
        return subType;
    }

    /**
     * 设置可用的，冻结的
     *
     * @param subType 可用的，冻结的
     */
    public void setSubType(String subType) {
        this.subType = subType;
    }

    /**
     * 获取支出，收入，冻结，解冻
     *
     * @return trade_type - 支出，收入，冻结，解冻
     */
    public String getTradeType() {
        return tradeType;
    }

    /**
     * 设置支出，收入，冻结，解冻
     *
     * @param tradeType 支出，收入，冻结，解冻
     */
    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    /**
     * @return amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * 获取当前可用的
     *
     * @return current_available - 当前可用的
     */
    public BigDecimal getCurrentAvailable() {
        return currentAvailable;
    }

    /**
     * 设置当前可用的
     *
     * @param currentAvailable 当前可用的
     */
    public void setCurrentAvailable(BigDecimal currentAvailable) {
        this.currentAvailable = currentAvailable;
    }

    /**
     * 获取当前冻结的
     *
     * @return current_frozen - 当前冻结的
     */
    public BigDecimal getCurrentFrozen() {
        return currentFrozen;
    }

    /**
     * 设置当前冻结的
     *
     * @param currentFrozen 当前冻结的
     */
    public void setCurrentFrozen(BigDecimal currentFrozen) {
        this.currentFrozen = currentFrozen;
    }

    /**
     * @return sign
     */
    public String getSign() {
        return sign;
    }

    /**
     * @param sign
     */
    public void setSign(String sign) {
        this.sign = sign;
    }

    /**
     * @return memo
     */
    public String getMemo() {
        return memo;
    }

    /**
     * @param memo
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * 获取是否隐藏(不对外显示)
     *
     * @return hidden - 是否隐藏(不对外显示)
     */
    public Boolean getHidden() {
        return hidden;
    }

    /**
     * 设置是否隐藏(不对外显示)
     *
     * @param hidden 是否隐藏(不对外显示)
     */
    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    /**
     * @return trade_at
     */
    public Date getTradeAt() {
        return tradeAt;
    }

    /**
     * @param tradeAt
     */
    public void setTradeAt(Date tradeAt) {
        this.tradeAt = tradeAt;
    }

    /**
     * @return created_at
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return created_by
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}