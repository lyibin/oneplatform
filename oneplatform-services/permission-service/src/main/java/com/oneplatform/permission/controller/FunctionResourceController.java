package com.oneplatform.system.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mendmix.common.annotation.ApiMetadata;
import com.mendmix.common.constants.PermissionLevel;
import com.mendmix.common.model.IdParam;
import com.mendmix.common.model.TreeModel;
import com.mendmix.common.util.BeanUtils;
import com.oneplatform.system.constants.BindingRelationType;
import com.oneplatform.system.dao.entity.FunctionResourceEntity;
import com.oneplatform.system.dto.FunctionResource;
import com.oneplatform.system.dto.ObjectRelation;
import com.oneplatform.system.dto.ResourceTreeModel;
import com.oneplatform.system.dto.param.FunctionResourceParam;
import com.oneplatform.system.service.FunctionResourceService;
import com.oneplatform.system.service.InternalRelationService;


@RestController
@RequestMapping("/functionResource")
public class FunctionResourceController {

	@Autowired
    private FunctionResourceService functionResourceService;
    @Autowired 
    private InternalRelationService relationService;

    
    @ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = true)
    @PostMapping("add")
    public IdParam<Integer> add(@RequestBody FunctionResourceParam param) {
        return functionResourceService.addFunctionResource(param);
    }

    
    @ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = true)
    @PostMapping("delete")
    public void delete(@RequestBody IdParam<Integer> param) {
        functionResourceService.deleteFunctionResource(param.getId());
    }

    
    @ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = true)
    @PostMapping("update")
    public void update(@RequestBody FunctionResourceParam param) {
        functionResourceService.updateFunctionResource(param);
    }

    
    @ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = true)
    @PostMapping("toggle")
    public void switchMenu(@RequestBody IdParam<Integer> param) {
        functionResourceService.switchMenu(param.getId());
    }

    
    @ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired, actionLog = true)
    @GetMapping("{id}")
    public FunctionResource get(@PathVariable("id") Integer id) {
        FunctionResourceEntity entity = functionResourceService.getFunctionResourceById(id);
        FunctionResource model = BeanUtils.copy(entity, FunctionResource.class);
        List<ObjectRelation> relations = relationService.findTargetBindingRelations(BindingRelationType.apiToFunc, id.toString());
		if(!relations.isEmpty()) {
			model.setBindApiId(relations.get(0).getFirstId());
		}
        return model;
    }

 
    
    @ApiMetadata(permissionLevel = PermissionLevel.LoginRequired)
    @GetMapping(value = "/tree")
    @ResponseBody
    public List<TreeModel> menuPermissions(@RequestParam(value = "button",required = false,defaultValue="true") boolean withButton) {
        List<ResourceTreeModel> resources = functionResourceService.getSystemMenuTree(withButton);  
		return TreeModel.build(resources).getChildren();
    }
    
    @ApiMetadata(permissionLevel = PermissionLevel.PermissionRequired)
    @ResponseBody
    public void syncModuleApis(@RequestParam("moduleId") Integer moduleId) {
    	
    }

}
