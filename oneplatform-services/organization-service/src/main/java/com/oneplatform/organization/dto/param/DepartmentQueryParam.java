package com.oneplatform.organization.dto.param;

/**
 * 
 * <br>
 * Class Name   : DepartmentQueryParam
 */
public class DepartmentQueryParam  {
	
	private String name;
	private String parentId;
	private Boolean isBunit;
	private Boolean isVirtual;
    private Boolean enabled;
 
    
    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the parentId
	 */
	public String getParentId() {
		return parentId;
	}

	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	
	public Boolean getIsBunit() {
		return isBunit;
	}

	public void setIsBunit(Boolean isBunit) {
		this.isBunit = isBunit;
	}

	public Boolean getIsVirtual() {
		return isVirtual;
	}

	public void setIsVirtual(Boolean isVirtual) {
		this.isVirtual = isVirtual;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	

}