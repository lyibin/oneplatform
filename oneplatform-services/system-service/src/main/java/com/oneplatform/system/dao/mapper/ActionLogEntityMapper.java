package com.oneplatform.system.dao.mapper;

import java.util.List;

import com.mendmix.logging.actionlog.ActionLogQueryParam;
import com.mendmix.mybatis.core.BaseMapper;
import com.oneplatform.system.dao.entity.ActionLogEntity;

public interface ActionLogEntityMapper extends BaseMapper<ActionLogEntity, String> {
	
	List<ActionLogEntity> findListByQueryParam(ActionLogQueryParam param);
}