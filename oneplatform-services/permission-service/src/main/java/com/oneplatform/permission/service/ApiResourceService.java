package com.oneplatform.system.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mendmix.common.MendmixBaseException;
import com.mendmix.common.model.IdParam;
import com.mendmix.common.model.Page;
import com.mendmix.common.model.PageParams;
import com.mendmix.common.util.AssertUtil;
import com.mendmix.common.util.BeanUtils;
import com.mendmix.mybatis.plugin.pagination.PageExecutor;
import com.oneplatform.system.dao.entity.ApiResourceEntity;
import com.oneplatform.system.dao.mapper.ApiResourceEntityMapper;
import com.oneplatform.system.dto.ApiResource;
import com.oneplatform.system.dto.param.ApiResourceParam;
import com.oneplatform.system.dto.param.ApiResourceQueryParam;

/**
 * <br>
 * Class Name : ApiResourceService
 *
 * @author jiangwei
 * @version 1.0.0
 * @date 2019年1月4日
 */
@Service
public class ApiResourceService {

	@Autowired
	private ApiResourceEntityMapper apiResourceMapper;
    /**
     * add
     * @param param
     * @return
     */
    public IdParam<Integer> addApiResource(ApiResourceParam param){

        if(apiResourceMapper.findByRequestUri(param.getModuleId(), param.getMethod(), param.getUri()) != null){
            throw new MendmixBaseException("接口已经存在["+param.getUri()+"]");
        }

        ApiResourceEntity entity = BeanUtils.copy(param, ApiResourceEntity.class);
        apiResourceMapper.insertSelective(entity);

        return new IdParam<>(entity.getId());
    }

    /**
     * 根据id删除apiResource
     * @param id
     */
    public void deleteApiResource(Integer id){

        AssertUtil.notNull(id,"参数缺失[id]");
        ApiResourceEntity oldEntity = apiResourceMapper.selectByPrimaryKey(id);
        AssertUtil.notNull(oldEntity,"删除接口不存在");
        apiResourceMapper.deleteByPrimaryKey(id);
    }

    /**
     * 更新apiResource
     * @param param
     */
    public void updateApiResource(ApiResourceParam param){

        AssertUtil.notNull(param.getId(),"参数缺失[id]");
        ApiResourceEntity oldEntity = apiResourceMapper.selectByPrimaryKey(param.getId());
        AssertUtil.notNull(oldEntity,"更新接口不存在");

        ApiResourceEntity sameUriEntity = apiResourceMapper.findByRequestUri(param.getModuleId(), param.getMethod(), param.getUri());
        if(sameUriEntity != null && !sameUriEntity.getId().equals(param.getId())){
            throw new MendmixBaseException("接口已经存在["+param.getUri()+"]");
        }

        ApiResourceEntity entity = BeanUtils.copy(param,oldEntity);
        apiResourceMapper.updateByPrimaryKey(entity);
    }

    /**
     * 接口激活状态变更
     * @param id
     */
    public void switchApiResource(Integer id){

        AssertUtil.notNull(id,"参数缺失[id]");

        ApiResourceEntity entity = apiResourceMapper.selectByPrimaryKey(id);
        AssertUtil.notNull(entity,"激活/禁用接口不存在");

        entity.setEnabled(!entity.getEnabled());
        apiResourceMapper.updateByPrimaryKeySelective(entity);
    }

    /**
     * 根据id查询接口信息
     * @param id
     * @return
     */
    public ApiResource getApiResourceById(Integer id){

        AssertUtil.notNull(id,"参数缺失[id]");
        return BeanUtils.copy(apiResourceMapper.selectByPrimaryKey(id),ApiResource.class);
    }

    /**
     * 根据查询参数查询接口列表
     * @param queryParam
     * @return
     */
    public List<ApiResource> listByQueryParam(ApiResourceQueryParam queryParam){
        return BeanUtils.copy(apiResourceMapper.findByQueryParam(queryParam),ApiResource.class);
    }


    public List<ApiResourceEntity> findByModuleId(Integer moduleId){
        ApiResourceQueryParam queryParam = new ApiResourceQueryParam();
        queryParam.setModuleId(moduleId);
        queryParam.setEnabled(true);
        //
        List<ApiResourceEntity> list = apiResourceMapper.findByQueryParam(queryParam);
        return list;
    }

    /**
     * 分页查询api资源列表
     * @param pageParams
     * @param example
     * @return
     */
    public Page<ApiResource> pageQry(PageParams pageParams, ApiResourceQueryParam example) {
    
        return PageExecutor.pagination(pageParams, new PageExecutor.ConvertPageDataLoader<ApiResourceEntity, ApiResource>() {
            @Override
            public ApiResource convert(ApiResourceEntity apiResourceEntity) {
                return BeanUtils.copy(apiResourceEntity,ApiResource.class);
            }

            @Override
            public List<ApiResourceEntity> load() {
                return apiResourceMapper.findByQueryParam(example == null ? new ApiResourceQueryParam() : example);
            }
        });
    }

    
    @Transactional(rollbackFor = Throwable.class)
	public void batchUpdateApiResource(Integer moduleId,List<ApiResourceEntity> entities) {
    	for (ApiResourceEntity entity : entities) {
			entity.setEnabled(true);
			entity.setDeleted(false);
		}
		ApiResourceQueryParam queryParam = new ApiResourceQueryParam();
		queryParam.setModuleId(moduleId);
		queryParam.setEnabled(true);
		List<ApiResourceEntity> hisEntitis = apiResourceMapper.findByQueryParam(queryParam);
		
		List<ApiResourceEntity> addList = new ArrayList<>(entities);
		addList.removeAll(hisEntitis);
		List<ApiResourceEntity> deleteList = new ArrayList<>(hisEntitis);
		deleteList.removeAll(entities);
		List<ApiResourceEntity> updateList = new ArrayList<>(entities);
		updateList.retainAll(hisEntitis);
		
		if(!updateList.isEmpty()) {
			Iterator<ApiResourceEntity> iterator = updateList.iterator();
			while (iterator.hasNext()) {
				ApiResourceEntity updateEntity = iterator.next();
				for (ApiResourceEntity entity : hisEntitis) {
					if (!entity.equals(updateEntity)){
						continue;
					}
					updateEntity.setId(entity.getId());
				}
			}
		}
		
		if(!addList.isEmpty()) {
			apiResourceMapper.insertList(addList);
		}
		
		if(!deleteList.isEmpty()) {
			for (ApiResourceEntity entity : deleteList) {
				entity.setEnabled(false);
				entity.setDeleted(true);
			}
			updateList.addAll(deleteList);
		}
		
		if(!updateList.isEmpty()) {
            for (ApiResourceEntity entity : updateList) {
            	apiResourceMapper.updateByPrimaryKeySelective(entity);
			}
		}
		
    }

}
